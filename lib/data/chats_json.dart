// danh sách story
const List chats_json = [
  {
    "img": "assets/images/girls/img_1.jpeg",
    "name": "Tuyết Nhi",
    "online": true,
    "story": false,
  },
  {
    "img": "assets/images/girls/img_2.jpeg",
    "name": "Hoải Thương",
    "online": true,
    "story": false,
  },
  {
    "img": "assets/images/girls/img_3.jpeg",
    "name": "Kiều Vân",
    "online": true,
    "story": true,
  },
  {
    "img": "assets/images/girls/img_4.jpeg",
    "name": "Xuân Hương",
    "online": false,
    "story": false,
  },
  {
    "img": "assets/images/girls/img_5.jpeg",
    "name": "Thanh Ngân",
    "online": true,
    "story": true,
  },
  {
    "img": "assets/images/girls/img_6.jpeg",
    "name": "Hải Yến",
    "online": false,
    "story": true,
  },
  {
    "img": "assets/images/girls/img_7.jpeg",
    "name": "Lucy",
    "online": true,
    "story": false,
  },
  {
    "img": "assets/images/girls/img_8.jpeg",
    "name": "Khánh Vy",
    "online": true,
    "story": true,
  },
  {
    "img": "assets/images/girls/img_9.jpeg",
    "name": "Ngọc Hà",
    "online": false,
    "story": true,
  },
  {
    "img": "assets/images/girls/img_10.jpeg",
    "name": "Linh Chi",
    "online": false,
    "story": true,
  },
];

// danh sách tin nhắn
List userMessages = [
  {
    "id": 1,
    "name": "Tuyết Nhi",
    "img": "assets/images/girls/img_1.jpeg",
    "online": true,
    "story": false,
    "message": "Bạn: Anh nhớ em.",
    "created_at": "11:25 pm"
  },
  {
    "id": 2,
    "name": "Hoải Thương",
    "img": "assets/images/girls/img_2.jpeg",
    "online": true,
    "story": false,
    "message": "ngày mai đi chơi đi",
    "created_at": "7:00 pm"
  },
  {
    "id": 3,
    "name": "Kiều Vân",
    "img": "assets/images/girls/img_3.jpeg",
    "online": false,
    "story": true,
    "message": "Bạn: hello mình là Hải",
    "created_at": "3:30 pm"
  },
  {
    "id": 4,
    "name": "Hải Yến",
    "img": "assets/images/girls/img_4.jpeg",
    "online": false,
    "story": false,
    "message": "Khi nào Deadline?",
    "created_at": "Sat 2:50 pm"
  },
  {
    "id": 5,
    "name": "Khánh Vy",
    "img": "assets/images/girls/img_5.jpeg",
    "online": true,
    "story": true,
    "message": "Bạn: okieeee",
    "created_at": "Fri 4:15 pm"
  },
  {
    "id": 6,
    "name": "Thanh Ngân",
    "img": "assets/images/girls/img_6.jpeg",
    "online": true,
    "story": true,
    "message": "137/55/35 Trần Đình xu, q1",
    "created_at": "Fri 3:40 am"
  },
];

