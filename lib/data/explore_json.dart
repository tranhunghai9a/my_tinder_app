const List explore_json = [
  {
    "img": "assets/images/girls/img_1.jpeg",
    "name": "Tuyết Nhi",
    "age": "22",
    "likes": ["xe phim", "Nấu ăn", "Vẽ"],
  },
  {
    "img": "assets/images/girls/img_2.jpeg",
    "name": "Hoải Thương",
    "age": "18",
    "likes": ["Du lịch", "Ăn văt"],
  },
  {
    "img": "assets/images/girls/img_3.jpeg",
    "name": "Kiều Vân",
    "age": "22",
    "likes": ["Xem Phim", "Chụp ảnh", "Du lịch"],
  },
  {
    "img": "assets/images/girls/img_4.jpeg",
    "name": "Xuân Hương",
    "age": "22",
    "likes": ["Ăn vặt", "Nghe nhạc", "coffee"],
  },
  {
    "img": "assets/images/girls/img_5.jpeg",
    "name": "Thanh Ngân",
    "age": "18",
    "likes": ["Gym", "Du lịch", "Xem phim"],
  },
  {
    "img": "assets/images/girls/img_6.jpeg",
    "name": "Hải Yến",
    "age": "19",
    "likes": ["Shopping", "Du lịch", "Mèo"],
  },
  {
    "img": "assets/images/girls/img_7.jpeg",
    "name": "Lucy",
    "age": "20",
    "likes": ["Ăn vặt", "Chụp hình", "Thú cưng"],
  },
  {
    "img": "assets/images/girls/img_8.jpeg",
    "name": "Khánh Vy",
    "age": "18",
    "likes": ["Nấu ăn", "Vẽ", "Chụp hình"],
  },
  {
    "img": "assets/images/girls/img_9.jpeg",
    "name": "Ngọc Hà",
    "age": "18",
    "likes": ["Bơi lội", "Du lịch"],
  },
  {
    "img": "assets/images/girls/img_10.jpeg",
    "name": "Linh Chi",
    "age": "19",
    "likes": ["Vẽ", "Xem phim"],
  },
];

