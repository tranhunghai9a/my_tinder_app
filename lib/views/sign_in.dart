import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../views/root_app.dart';
import '../validation/validation.dart';
import '../theme/colors.dart';


class SignIn extends StatefulWidget {
  static const route= '/signIn';
  @override
  State<StatefulWidget> createState() {
    return SignInState();
  }

}

class SignInState extends State<StatefulWidget> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('My Tinder App'),
        backgroundColor: primary_one,),
      body: SignInField(),
    );
  }

}
class SignInField extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SignInFieldState();
  }

}

class SignInFieldState extends State<StatefulWidget> with CommonValidation {
  final formKey = GlobalKey<FormState>();
  String phoneNumber;


  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
        child: Form(
          key: formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Chào Mừng đến Tinder",
                style: TextStyle(fontWeight: FontWeight.bold,
                    fontSize: 30,
                    color: primary_one),),
              Container(margin: EdgeInsets.only(top: 30.0),),
              SvgPicture.asset("assets/images/explore_active_icon.svg",
                height: size.height * 0.3,),
              Container(margin: EdgeInsets.only(top: 30.0),),
              fieldPhoneNumber(),
              Container(margin: EdgeInsets.only(top: 30.0),),
              loginButton()
            ],
          ),
        )

    );
  }

  Widget fieldPhoneNumber() {
    return TextFormField(
      keyboardType: TextInputType.phone,
      decoration: InputDecoration(
          icon: Icon(Icons.phone),
          labelText: 'Nhập Số điện thoại',
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0),
          )
      ),
      validator: validatePhone,
      onSaved: (value) {
        phoneNumber = value as String;
      },
    );
  }




  Widget loginButton() {
    return ElevatedButton(
      onPressed: () {
        if (formKey.currentState.validate()) {
          // Call API Authentication from Backend
          formKey.currentState.save();
          Navigator.of(context).pushReplacementNamed(RootPage.route);
        }
      },
      child: Text('Đăng Nhập'),
      style: ElevatedButton.styleFrom(primary: primary_one),
    );
  }

}