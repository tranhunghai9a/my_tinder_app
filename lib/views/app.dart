import 'package:flutter/material.dart';
import './sign_in.dart';
import './root_app.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/': (context) => SignIn(),
        '/signIn': (context) => SignIn(),
        '/app': (context) => RootPage()
      },
      initialRoute: '/',
    );
  }

}
